# OpenML dataset: space_ga

https://www.openml.org/d/44985

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Data Description**

The dataset contains 3,107 observations on U.S. county votes cast in the 1980 presidential election.

Given population, education levels in the population, number of owned houses, incomes and coordinates, the goal is to predict the number of votes per population (logarithmic).

**Attribute Description**

1. *ln_votes_pop* - logarithm of total number of votes - logarithm of populaiton (ln(votes/pop)), target feature
2. *pop* - the population in each county of 18 years of age or older
3. *education* - the population in each county with a 12th grade or higher education
4. *houses* - the number of owner-occupied housing units
5. *income* - the aggregate income
6. *xcoord* - X spatial coordinate of the county
7. *ycoord* - Y spatial coordinate of the county

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/44985) of an [OpenML dataset](https://www.openml.org/d/44985). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/44985/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/44985/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/44985/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

